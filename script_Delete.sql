--Delete all the tables

DROP VIEW vPersonne;
DROP VIEW vPersonnel;
DROP TABLE Taille_Poids ;
DROP TABLE DosageMed ;
DROP TABLE Medicament_Espece ;
DROP TABLE Traitements ;
DROP TABLE Veterinaires ;
DROP TABLE Assistants ;
DROP TABLE Animal ;
DROP TABLE Especes; 
DROP TABLE ClasseEspeces ;
DROP TABLE Clients ;
DROP TABLE Medicaments ;